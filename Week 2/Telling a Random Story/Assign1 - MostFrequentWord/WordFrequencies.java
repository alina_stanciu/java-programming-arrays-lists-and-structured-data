import java.util.*;
import edu.duke.*;

public class WordFrequencies {

    private ArrayList<String> myWords;
    private ArrayList<Integer> myFreqs;

    public WordFrequencies() {
        
        myWords = new ArrayList<String>();
        myFreqs = new ArrayList<Integer>();
    }
    
    public void findUnique() {
        
        myWords.clear();
        myFreqs.clear();
        
        FileResource fr = new FileResource();
        
        for(String s: fr.words()) {
            s = s.toLowerCase();
            int index = myWords.indexOf(s);
            
            if(index == -1) {
                myWords.add(s);
                myFreqs.add(1);
            } else {
                int freqs = myFreqs.get(index);
                myFreqs.set(index, freqs + 1);
            }
        }
    }
    
    public void tester() {
        
        findUnique();
        int countUnique = 0;
        
        for(int index = 0; index < myWords.size(); index++) {
            System.out.println(myFreqs.get(index) + "\t" + myWords.get(index));
            countUnique++;
        }
        System.out.println("Number of unique words: " + countUnique);
        
        int indexOfMax = findIndexOfMax();
        System.out.println("The word that occurs most often and its number of appereances are: "+ 
                           myWords.get(indexOfMax) + "\t" + myFreqs.get(indexOfMax));
    }
    
    public int findIndexOfMax() {
        
        int max = myFreqs.get(0);
        int maxIndex = 0;
        
        for(int index = 0; index < myFreqs.size(); index++) {
            
            if(myFreqs.get(index) > max) {
                max = myFreqs.get(index);
                maxIndex = index;
            }
        }
        return maxIndex;
    }
}