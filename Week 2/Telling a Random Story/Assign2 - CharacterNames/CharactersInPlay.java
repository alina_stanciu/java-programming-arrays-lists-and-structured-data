import java.util.*;
import edu.duke.*;

public class CharactersInPlay {

    private ArrayList<String> myNames;
    private ArrayList<Integer> myFreqs;
    
    public CharactersInPlay() {
        
        myNames = new ArrayList<String>();
        myFreqs = new ArrayList<Integer>();
    }
    
    public void update(String person) {
        
       String personLower = person.toLowerCase();
       int index = myNames.indexOf(personLower);
       
       if(index == -1) {
           myNames.add(personLower);
           myFreqs.add(1);
       } else {
           int freqs = myFreqs.get(index);
           myFreqs.set(index, freqs + 1);
       }
    }
    
    public void findAllCharacters() {
        
        myNames.clear();
        myFreqs.clear();
        FileResource fr = new FileResource();
        int index = 0;
        
        for(String s: fr.lines()) {
            index = s.indexOf(".");
            String substring = s.substring(0, index + 1);
            update(substring);
        }
    }
    
    public void tester() {
        
        findAllCharacters();
        
        /*for(int index = 0; index < myNames.size(); index++) {
            
            if(myFreqs.get(index) > 1) {
                System.out.println(myNames.get(index) + "\t" + myFreqs.get(index));
            }
        }*/
        
        charactersWithNumParts(10, 15);
    }
    
    public void charactersWithNumParts(int num1, int num2) {
        
        for(int index = 0; index < myNames.size(); index++) {
            if(myFreqs.get(index) >= num1 && myFreqs.get(index) <= num2) {
                System.out.println(myNames.get(index) + "\t" + myFreqs.get(index));
            }
        }
    }
}