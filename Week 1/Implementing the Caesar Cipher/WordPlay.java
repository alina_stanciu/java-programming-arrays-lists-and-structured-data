import edu.duke.*;

public class WordPlay {

    public boolean isVowel(char ch) {
        
        if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || 
           ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
            return true;
        } else {
            return false;
        }
    }
    
    public void testIsVowel() {
        
        System.out.println("Testing \"a\": " + isVowel('a'));
        System.out.println("Testing \"m\": " + isVowel('m'));
        System.out.println("Testing \"O\": " + isVowel('O'));
        System.out.println("Testing \"R\": " + isVowel('R'));
    }
    
    public String replaceVowels(String phrase, char ch) {
        
        StringBuilder sb = new StringBuilder();
        
        for(int index = 0; index < phrase.length(); index++) {
            char getChar = phrase.charAt(index);
            
            if(isVowel(getChar) == true) {
                sb.insert(index, ch);
            } else {
                sb.insert(index, getChar);
            }
        }
        System.out.println(sb);
        
        return sb.toString();
    }
    
    public void testReplaceVowels() {
        
        replaceVowels("Hello World", '*');
        replaceVowels("ALELUIA", '*');
    }
    
    public String emphasize(String phrase, char ch) {
        
        StringBuilder sb = new StringBuilder();
        
        char x = Character.toLowerCase(ch);
        
        for(int index = 0; index < phrase.length(); index++) {
            char getChar = phrase.charAt(index);
            char lowGetChar = Character.toLowerCase(getChar);
            
            if(index % 2 == 0 && x == lowGetChar) {
                sb.insert(index, '*');
            } else if (index % 2 == 1 && x == lowGetChar) {
                sb.insert(index, '+');
            } else {
                sb. insert(index, getChar);
            }
        }
        System.out.println(sb);
        
        return sb.toString();
    }
    
    public void testEmphasize() {
        
        emphasize("dna ctgaaactga", 'a');
        emphasize("Mary Bella Abracadabra", 'a');
    }
}