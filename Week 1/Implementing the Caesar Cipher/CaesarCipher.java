import edu.duke.*;

public class CaesarCipher {

    public String encrypt(String input, int key) {

        StringBuilder encrypted = new StringBuilder(input);
        
        String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String alphabetL = "abcdefghijklmnopqrstuvwxyz";
        
        String shiftedAlphabetU = alphabetU.substring(key) + alphabetU.substring(0,key);
        String shiftedAlphabetL = alphabetL.substring(key) + alphabetL.substring(0,key);
        
        for(int index = 0; index < encrypted.length(); index++) {
            char currentChar = encrypted.charAt(index);
            
            if(Character.isUpperCase(currentChar) == true) {
                int indexU = alphabetU.indexOf(currentChar);
                
                if(indexU != -1) {
                    char newChar = shiftedAlphabetU.charAt(indexU);
                    encrypted.setCharAt(index, newChar);
                }
            } else {
                int indexL = alphabetL.indexOf(currentChar);
                
                if(indexL != -1) {
                    char newChar = shiftedAlphabetL.charAt(indexL);
                    encrypted.setCharAt(index, newChar);
                }
            }
        }
        //System.out.println(encrypted);
        return encrypted.toString();
    }
    
    public void testCaesar() {
        
        int key = 17;
        FileResource fr = new FileResource();
        String message = fr.asString();
        String encrypted = encrypt(message, key);
        System.out.println("The key is " + key);
        System.out.println(encrypted);
        String decrypted = encrypt(encrypted, 26-key);
        System.out.println(decrypted);
    }
    
    public String encryptTwoKeys(String input, int key1, int key2) {
        
        StringBuilder encrypted = new StringBuilder(input);
        
        String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String alphabetL = "abcdefghijklmnopqrstuvwxyz";
        
        String shiftedAlphabetU1 = alphabetU.substring(key1) + alphabetU.substring(0, key1);
        String shiftedAlphabetL1 = alphabetL.substring(key1) + alphabetL.substring(0, key1);
        
        String shiftedAlphabetU2 = alphabetU.substring(key2) + alphabetU.substring(0, key2);
        String shiftedAlphabetL2 = alphabetL.substring(key2) + alphabetL.substring(0, key2);
        
        for(int index = 0; index < encrypted.length(); index++) {
            char newChar = encrypted.charAt(index);
            
            if(Character.isUpperCase(newChar) == true) {
                int indexU = alphabetU.indexOf(newChar);
                
                if(indexU != -1 && index % 2 == 0) {
                    char currChar = shiftedAlphabetU1.charAt(indexU);
                    encrypted.setCharAt(index, currChar);
                } else if(indexU != -1 && index % 2 == 1) {
                    char currChar = shiftedAlphabetU2.charAt(indexU);
                    encrypted.setCharAt(index, currChar);
                }
            } else {
                int indexL = alphabetL.indexOf(newChar);
                
                if(indexL != -1 && index % 2 == 0) {
                    char currChar = shiftedAlphabetL1.charAt(indexL);
                    encrypted.setCharAt(index, currChar);
                } else if(indexL != -1 && index % 2 == 1) {
                    char currChar = shiftedAlphabetL2.charAt(indexL);
                    encrypted.setCharAt(index, currChar);
                }
            }
        }
        System.out.println("Encrypted message:" + "\n" + encrypted);
        
        return encrypted.toString();
    }
    
    public void testEncryptTwoKeys() {
        
        String message = "At noon be in the conference room with your hat on for a surprise party. YELL LOUD!";
        int key1 = 8;
        int key2 = 21;
        System.out.println("Decrypted message:" + "\n" + message);
        System.out.println("key1: " + key1 + " and key2: " + key2); 
        encryptTwoKeys(message, key1, key2);
    }
}