import edu.duke.*;

public class CaesarBreaker {

    public int[] countLetters(String message) {
        
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        
        for(int firstIndex = 0; firstIndex < message.length(); firstIndex++) {
            char currentChar = Character.toLowerCase(message.charAt(firstIndex));
            int secondIndex = alphabet.indexOf(currentChar);
            
            if(secondIndex != -1) {
                counts[secondIndex]++;
            }
        }
        return counts;
    }
    
    public int indexOfMax(int[] values) {
        
        int maxIndex = 0;
        int maxElement = 0;
        
        for(int index = 0; index < values.length; index++) {
            if(values[index] > maxElement) {
                maxIndex = index;
                maxElement = values[index];
            }
        }
        return maxIndex;
    }
    
    public String decrypt(String encrypted, int key) {
        
        CaesarCipher cc = new CaesarCipher();
        String decrypted = cc.encrypt(encrypted, 26 - key);
        
        return decrypted;
    }
    
    public void testDecrypt() {
        
        int key = 23;
        FileResource fr = new FileResource("data/wordsLotsOfEs.txt");
        String message = fr.asString();
        CaesarCipher cc = new CaesarCipher();
        String decrypted = cc.encrypt(message, 26 - key);
        System.out.println("The key is: " + key + "\n" + decrypted);
    }
    
    public String halfOfString(String message, int start) {
        
        String result = new String();
        
        for(int index = start; index < message.length(); index += 2) {
            result = result + message.charAt(index);
        }
        return result;
    }
    
    public void testHalfOfString() {
        
       FileResource fr = new FileResource("data/wordsLotsOfEs.txt");
       String message = fr.asString();
       
       System.out.println(message);
       System.out.println("The half of string that starts with 0 is:\n" + halfOfString(message, 0));
       System.out.println("The half of string that starts with 1 is:\n" + halfOfString(message, 1));
       
       System.out.println(halfOfString("Qbkm Zgis", 0));
       System.out.println(halfOfString("Qbkm Zgis", 1));
    }
    
    public int getKey(String s) {
        
        int[] freqs = countLetters(s);
        int maxIndex = indexOfMax(freqs);
        int decryptKey = maxIndex - 4;
        
        if(maxIndex < 4) {
            decryptKey = 26 - (4 - maxIndex);
        }
        return decryptKey;
    }
    
    public String decryptTwoKeys(String encrypted) {
        
        CaesarCipher cc = new CaesarCipher();
        
        String zeroHalf = halfOfString(encrypted, 0);
        String oneHalf = halfOfString(encrypted, 1);
        
        StringBuilder decrypt_two = new StringBuilder(encrypted);
        
        int firstKey = getKey(zeroHalf);
        int secondKey = getKey(oneHalf);
        
        String encrypt_zeroHalf = cc.encrypt(zeroHalf, (26 - firstKey));
        String encrypt_oneHalf = cc.encrypt(oneHalf, (26 - secondKey));
        
        for(int index = 0; index < zeroHalf.length(); index++) {
            decrypt_two.setCharAt((2*index), encrypt_zeroHalf.charAt(index));
        }
        
        for(int index = 0; index < oneHalf.length(); index++) {
            decrypt_two.setCharAt((2*index) + 1, encrypt_oneHalf.charAt(index));
        }
        System.out.println("The first key is: " + firstKey);
        System.out.println("The second key is: " + secondKey);
        
        return decrypt_two.toString();
    }
    
    public void testDecryptTwoKeys() {
        
       FileResource fr = new FileResource("data/mysteryTwoKeysQuiz.txt");
       String message = fr.asString();
       //String message = "Aal uttx hm aal Qtct Fhljha pl Wbdl. Pvxvxlx!";
       String decrypted_message = decryptTwoKeys(message); 
       System.out.println("Encrypted message: " + "\n" + message);
       System.out.println("Decrypted message:" + "\n" + decrypted_message);
    }
}