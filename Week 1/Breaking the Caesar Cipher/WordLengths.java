import edu.duke.*;

public class WordLengths {

    public void countWordLengths(FileResource resource, int[] counts) {
        
        for(String word: resource.words()) {
            int wordLength = word.length();
            
            if(!Character.isLetter(word.charAt(word.length() - 1))) {
                wordLength--;
            }
            
            if(wordLength >= counts.length) {
                wordLength = counts.length - 1;
            }
            
            if(wordLength > 0) {
                counts[wordLength]++;
            }
        }
    }
    
    public void testCountWordLengths() {
        
        FileResource fr = new FileResource("data/manywords.txt");
        int[] counts = new int[31];
        
        countWordLengths(fr, counts);
        for (int index = 0; index < counts.length; index++) {
            
            if (counts[index] != 0) {
                System.out.println(counts[index] + " word(s) of length " + index);
            }
        }
        
        int maxIndex = indexOfMax(counts);
        System.out.println("The most common word length in the file is: " + maxIndex);
    }
    
    public int indexOfMax(int[] values) {
        
        int maxIndex = 0;
        int maxElement = 0;
        
        for(int index = 0; index < values.length; index++) {
            if(values[index] > maxElement) {
                maxIndex = index;
                maxElement = values[index];
            }
        }
        return maxIndex;
    }
}