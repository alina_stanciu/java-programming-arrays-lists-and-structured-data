import edu.duke.*;

public class TestCaesarCipherTwo {

    public int[] countLetters(String message) {
        
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        
        for(int firstIndex = 0; firstIndex < message.length(); firstIndex++) {
            char currentChar = Character.toLowerCase(message.charAt(firstIndex));
            int secondIndex = alphabet.indexOf(currentChar);
            
            if(secondIndex != -1) {
                counts[secondIndex]++;
            }
        }
        return counts;
    }
    
    public int maxIndex(int[] values) {
        
        int maxIndex = 0;
        int maxElement = 0;
        
        for(int index = 0; index < values.length; index++) {
            if(values[index] > maxElement) {
                maxIndex = index;
                maxElement = values[index];
            }
        }
        return maxIndex;
    }
    
    public String halfOfString(String message, int start) {
        
        String result = new String();
        
        for(int index = start; index < message.length(); index += 2) {
            result = result + message.charAt(index);
        }
        return result;
    }
    
    public int getKey(String s){
        int[] freqs = countLetters(s);
        int maxIndex = maxIndex(freqs);
        int decryptKey = maxIndex - 4;
        
        if (maxIndex < 4){
            decryptKey = 26 - (4 - maxIndex);
        }
        
        return decryptKey;
    }
    
    public String breakCaesarCipher(String input) {
        
        int decryptKeyOne = getKey(halfOfString(input, 0));
        int decryptKeyTwo = getKey(halfOfString(input, 1));
        
        System.out.println("First key: " + decryptKeyOne);
        System.out.println("Second key: " + decryptKeyTwo);
        
        CaesarCipherTwo cc = new CaesarCipherTwo(decryptKeyOne, decryptKeyTwo);
        
        return cc.decrypt(input);
    }
    
    public void simpleTests() {
        
        FileResource fr = new FileResource();
        String frAsString = fr.asString();
        CaesarCipherTwo cc = new CaesarCipherTwo(17, 3);
        
        String frEncrypted = cc.encrypt(frAsString);
        System.out.println("The encrypted message is:" + "\n" + frEncrypted);
        
        String frDecrypted = cc.decrypt(frEncrypted);
        System.out.println("The decrypted message is:" + "\n" + frDecrypted);
    }
}