public class CaesarCipherTwo {

    private String alphabetUpper;
    private String alphabetLower;
    private String shiftedAlphOneUpper;
    private String shiftedAlphOneLower;
    private String shiftedAlphTwoUpper;
    private String shiftedAlphTwoLower;
    private int firstKey;
    private int secondKey;
    
    public CaesarCipherTwo(int keyOne, int keyTwo) {
        
        alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        alphabetLower = alphabetUpper.toLowerCase();
        shiftedAlphOneUpper = alphabetUpper.substring(keyOne)+ alphabetUpper.substring(0,keyOne);
        shiftedAlphOneLower = alphabetLower.substring(keyOne)+ alphabetLower.substring(0,keyOne);
        shiftedAlphTwoUpper = alphabetUpper.substring(keyTwo)+ alphabetUpper.substring(0,keyTwo);
        shiftedAlphTwoLower = alphabetLower.substring(keyTwo)+ alphabetLower.substring(0,keyTwo);
        firstKey = keyOne;
        secondKey = keyTwo;
    }
    
    public String encrypt(String input) {
        
       StringBuilder encrypted = new StringBuilder(input);
        
       for (int firstIndex = 0; firstIndex < encrypted.length(); firstIndex += 2){
           char currChar = encrypted.charAt(firstIndex);
            
           if ((firstIndex % 2 == 0) && (Character.isLowerCase(currChar))) {
               int secondIndex = alphabetLower.indexOf(currChar);
                
               if (secondIndex != 0) {
                   char newChar = shiftedAlphOneLower.charAt(secondIndex);
                   encrypted.setCharAt(firstIndex, newChar);
               }
                
           } else if ((firstIndex %2 == 0) && (Character.isUpperCase(currChar))) {
               int secondIndex = alphabetUpper.indexOf(currChar);
                
               if(secondIndex != 0) {
                   char newChar = shiftedAlphOneUpper.charAt(secondIndex);
                   encrypted.setCharAt(firstIndex,newChar);
               }
           }
       }
    
       for (int firstIndex = 1; firstIndex <encrypted.length();firstIndex+=2) {
            char currChar = encrypted.charAt(firstIndex);
            
            if ((firstIndex %2 != 0) && (Character.isLowerCase(currChar))) {
                int secondIndex = alphabetLower.indexOf(currChar);
                
            if (secondIndex != 0) {
                char newChar = shiftedAlphTwoLower.charAt(secondIndex);
                encrypted.setCharAt(firstIndex,newChar);
            }
            } else if ((firstIndex %2 != 0) && (Character.isUpperCase(currChar))) {
                int secondIndex = alphabetUpper.indexOf(currChar);
                
                if (secondIndex != 0) {
                    char newChar = shiftedAlphTwoUpper.charAt(secondIndex);
                    encrypted.setCharAt(firstIndex,newChar);
                }
            }
       }         
       return encrypted.toString();   
    }
    
    public String decrypt(String input) {
        
        CaesarCipherTwo cc =  new CaesarCipherTwo(26 - firstKey, 26 - secondKey);
        String decrypted = cc.encrypt(input);
        
        return decrypted;
    }
    
    public void testEncrypt(){
        String message = "Can you imagine life WITHOUT the internet AND computers in your pocket?";
        String encryptedMessage = encrypt(message);
        System.out.println("Encrypted message:" + "\n" + encryptedMessage);
        System.out.println("Decrypted message: " + "\n" + decrypt(encryptedMessage));
    }
}