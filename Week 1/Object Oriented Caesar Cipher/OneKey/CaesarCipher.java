public class CaesarCipher {

    private String alphabet;
    private String shiftedAlphabet;
    private int mainKey;
    
    public CaesarCipher(int key) {
        
        alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        shiftedAlphabet = alphabet.substring(key) + alphabet.substring(0, key);
        mainKey = key;
    }
    
    public String encrypt(String input) {
        
        StringBuilder sb = new StringBuilder(input);
        String alphabetLower = alphabet.toLowerCase();
        String shiftAlphLower = shiftedAlphabet.toLowerCase();
        
        for(int firstIndex = 0; firstIndex < sb.length(); firstIndex++) {
            char getChar = sb.charAt(firstIndex);
            int secondIndex = alphabet.indexOf(getChar);
            int indexLower = alphabetLower.indexOf(getChar);
            
            if(Character.isLowerCase(getChar)) {
                
                if(indexLower != -1) {
                    getChar = shiftAlphLower.charAt(indexLower);
                    sb.setCharAt(firstIndex, getChar);
                }
            } else {
                if(secondIndex != -1) {
                    getChar = shiftedAlphabet.charAt(secondIndex);
                    sb.setCharAt(firstIndex, getChar);
                }
            }
        }
        return sb.toString();
    }
    
    public String decrypt(String input) {
        
        CaesarCipher cc = new CaesarCipher(26 - mainKey);
        String decrypt = cc.encrypt(input);
        
        return decrypt;
    }
    
    public void testEncrypt(){
        String message = "Can you imagine life WITHOUT the internet AND computers in your pocket?";
        String encryptedMessage = encrypt(message);
        System.out.println("Encrypted message:" + "\n" + encryptedMessage);
        System.out.println("Decrypted message:" + "\n" + decrypt(encryptedMessage));
    }
}