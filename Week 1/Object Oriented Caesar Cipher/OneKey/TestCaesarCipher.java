import edu.duke.*;

public class TestCaesarCipher {

    public int[] countLetters(String message) {
        
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        
        for(int firstIndex = 0; firstIndex < message.length(); firstIndex++) {
            char currentChar = Character.toLowerCase(message.charAt(firstIndex));
            int secondIndex = alphabet.indexOf(currentChar);
            
            if(secondIndex != -1) {
                counts[secondIndex]++;
            }
        }
        return counts;
    }
    
    public int maxIndex(int[] values) {
        
        int maxIndex = 0;
        int maxElement = 0;
        
        for(int index = 0; index < values.length; index++) {
            if(values[index] > maxElement) {
                maxIndex = index;
                maxElement = values[index];
            }
        }
        return maxIndex;
    }
    
    public void simpleTests() {
        
        FileResource fr = new FileResource();
        String frAsString = fr.asString();
        CaesarCipher cc = new CaesarCipher(18);
        
        String frEncrypted = cc.encrypt(frAsString);
        System.out.println("The encrypted message is:" + "\n" + frEncrypted);
        
        String frDecrypted = cc.decrypt(frEncrypted);
        System.out.println("The decrypted message is:" + "\n" + frDecrypted);
        
        System.out.println("Breaking the Caesar Cipher: " + "\n" + breakCaesarCipher(frEncrypted));
    }
    
    public String breakCaesarCipher(String input) {
        
        int[] freqs = countLetters(input);
        int maxIndex = maxIndex(freqs);
        int decryptKey = maxIndex - 4;
        
        if(maxIndex < 4) {
            decryptKey = 26 - (4 - maxIndex);
        }
        
        CaesarCipher cc = new CaesarCipher(decryptKey);
        
        return cc.decrypt(input);
    }
}